import { Component, OnInit } from '@angular/core';
import { Citation } from '../../shared/citations.model';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-quote-details',
  templateUrl: './quote-details.component.html',
  styleUrls: ['./quote-details.component.css']
})
export class QuoteDetailsComponent implements OnInit {
  citation!: Citation;
  citationId = '';
  author: string = '';
  category: string = '';
  quoteText: string = '';

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) =>{
      this.citationId = params['id'];
    });
    this.http.get<Citation>(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/citations/${this.citationId}.json`).subscribe(result =>{
      this.citation = result;
    })
  }





}
