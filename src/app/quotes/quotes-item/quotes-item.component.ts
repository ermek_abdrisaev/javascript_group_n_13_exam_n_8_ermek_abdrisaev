import { Component, Input, OnInit } from '@angular/core';
import { Citation } from '../../shared/citations.model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-quotes-item',
  templateUrl: './quotes-item.component.html',
  styleUrls: ['./quotes-item.component.css']
})
export class QuotesItemComponent implements OnInit {
  @Input() citation!: Citation;

  constructor( private router: Router, private http: HttpClient) { }

  ngOnInit(): void {
  }

  onDelete(id: string){
    this.http.delete(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/citations/${id}.json`).subscribe();
    void this.router.navigate(['/']);
  }

}
