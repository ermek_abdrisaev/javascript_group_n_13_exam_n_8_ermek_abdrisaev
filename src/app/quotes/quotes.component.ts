import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Citation } from '../shared/citations.model';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
  citations!: Citation[];
  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) =>{
      console.log(params['quote']);
    });
    this.http.get<{[id: string]: Citation}>('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/citations.json')
      .pipe(map(result =>{
      if(result === null){
        return [];
      }
      return Object.keys(result).map(id =>{
        const quotesData = result[id];

        return new Citation(
          id,
          quotesData.author,
          quotesData.category,
          quotesData.text,
        );
      });
    }))
    .subscribe( citations =>{
      this.citations = citations
    });
  }

}
