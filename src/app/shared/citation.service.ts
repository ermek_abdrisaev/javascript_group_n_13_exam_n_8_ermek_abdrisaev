import { EventEmitter } from '@angular/core';
import { Citation } from './citations.model';

export class CitationService {
  citationsChage = new EventEmitter<Citation>();

  private citations: Citation[] = [];

  getCitations(){
    return this.citations.slice();
  }


}
