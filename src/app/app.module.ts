import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { QuotesComponent } from './quotes/quotes.component';
import { HomeComponent } from './home/home.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FormsModule } from '@angular/forms';
import { LinksComponent } from './links/links.component';
import { QuotesItemComponent } from './quotes/quotes-item/quotes-item.component';
import { HttpClientModule } from '@angular/common/http';
import { QuoteDetailsComponent } from './quotes/quote-details/quote-details.component';
import { NotFoundComponent } from './not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    NewQuoteComponent,
    QuotesComponent,
    HomeComponent,
    ToolbarComponent,
    LinksComponent,
    QuotesItemComponent,
    QuoteDetailsComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
