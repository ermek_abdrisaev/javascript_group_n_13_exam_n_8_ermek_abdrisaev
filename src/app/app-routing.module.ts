import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { NotFoundComponent } from './not-found.component';
import { QuoteDetailsComponent } from './quotes/quote-details/quote-details.component';

const routes: Routes = [
  {path: '', component: HomeComponent, children: [
      {path: 'quotes', component: HomeComponent},
      {path: ':quote', component: QuoteDetailsComponent},
    ]},
  {path: 'details/:id/:edit', component: NewQuoteComponent},
  {path: 'new', component: NewQuoteComponent},
  {path: "**", component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
