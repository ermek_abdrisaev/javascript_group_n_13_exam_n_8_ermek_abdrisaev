import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Citation } from '../shared/citations.model';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent implements OnInit {
  @Output() newCitation = new EventEmitter<Citation>();
  citation!: Citation;
  citationId = '';
  author = '';
  category = '';
  quoteText = '';

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) =>{
      this.citationId = params['id'];
      this.http.get<Citation>(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/citations/${params['id']}.json`)
        .subscribe(result =>{
          this.citation = result;
          this.author = this.citation.author;
          this.category = this.citation.category;
          this.quoteText = this.citation.text;
        })
    });
  }

  onSubmit(){
    if(this.citationId === undefined){
      const author = this.author;
      const category = this.category;
      const text = this.quoteText;
      const body = {author, text, category};
      this.http.post('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/citations.json', body).subscribe();
    }else{
      const author = this.author;
      const category = this.category;
      const text = this.quoteText;
      const body = {author, text, category};
      this.http.put<Citation>(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/citations/${this.citationId}.json`, body).subscribe();
    }
  }
}
